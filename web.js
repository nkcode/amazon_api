'use strict';
// web.js
var _ = require('underscore');
var fs = require('fs');
var express = require("express");
var logfmt = require("logfmt");
var app = express();

app.use(logfmt.requestLogger());

app.get('/', function(req, res) {
  fs.readFile("views/index.html",'utf8',function(err,data) {
  	res.send(data)
  })
});



var util = require('util'),
    OperationHelper = require('apac').OperationHelper;

var opHelper = new OperationHelper({
    awsId:     'AKIAJSWKH4OWMKRVNG2Q',
    awsSecret: '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',
    assocId:   'httppennykcom-20'
    // xml2jsOptions: an extra, optional, parameter for if you want to pass additional options for the xml2js module. (see https://github.com/Leonidas-from-XIV/node-xml2js#options)
});


var xml2js = require('xml2js');
var parser = new xml2js.Parser();

var product_price_lookup_template;

fs.readFile("views/product_price_lookup.html",'utf8',function(err,data) {
  product_price_lookup_template = _.template(data);
})

app.get('/product_price_lookup.:format',function(req,res) {

	opHelper.execute('ItemSearch', {
	  'SearchIndex': 'All',
	  'Keywords': req.query.product_name,
	  'ResponseGroup': 'ItemAttributes,Offers'
	}, function(err, results) { // you can add a third parameter for the raw xml response, "results" here are currently parsed using xml2js

    parser.parseString(results, function (err, result) {

    	  var items = result["ItemSearchResponse"]["Items"][0]["Item"];
    	  console.log("results: "+items.length)
    	  console.log(items)
    	  switch(req.params.format) {
    	  	case 'html':
	    	    var html = product_price_lookup_template({items:items}) 
	    	    console.log(JSON.stringify(items[0]["ItemAttributes"][0]["ListPrice"][0]["FormattedPrice"][0],null,2))
	          res.send( html ) 
	          break;
	        case 'json':
	          res.send(JSON.stringify(items))
	          break;
    	  }
    });

	});

})


var port = Number(process.env.PORT || 8080);
app.listen(port, function() {
  console.log("Listening on " + port);
});

