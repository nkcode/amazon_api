#!/bin/bash
cp ./manifest.json ./src/manifest.json
echo "generating extension on mac"
"/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" --pack-extension=./src --pack-extension-key=./extension.pem --no-message-box
mv src.crx amazon-plugin.crx
echo "new plugin is in amazon-plugin.crx"

#cleaning up
rm ./src/manifest.json
echo "done"
