#hautomatic extension
cp ./extension.pem ./src/key.pem
cp ./manifest.json ./src/manifest.json
cd src
rm ~/tmp/amazon-plugin.zip
zip -r ~/tmp/amazon-plugin.zip *
cd ..
rm ./src/key.pem

#cleaning up

rm ./src/manifest.json