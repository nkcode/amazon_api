var factory = function(
    _,
    SimpleFuture
) {
    var Lang = function() {};

    Lang.prototype = {
        noop: function() {},
        partiallyApply: function() {
            var args = _.toArray(arguments);
            var fun = args.shift();
            return function() {
                var additionalArgs = _.toArray(arguments);
                return fun.apply(null, args.concat(additionalArgs));
            }
        },
        params: function(obj) {
            var scopedRequire = new ScopedRequire(obj);
            return scopedRequire;
        },
        returns: function(what, cb) {
            return function(err) {
                if (err) {
                    cb(err);
                    return;
                }
                cb(null, what);
            }
        },
        /**
         * This check has side effects.
         * Given cb, potentialErr, calls cb(potentialErr) and returns
         * true iff potentialErr is truthy.
         *
         * Usage is within a callback, to propagate errors and kill further
         * execution of async functions:
         *
         *   function(cb) {
         *     this.someAsyncMethod(val, function(err, result) {
         *        if ($lang.cbOnErr(cb, err)) {
         *            return;
         *        }
         *        // Else do something interesting...
         *     });
         *   }
         *
         * Since cbOnErr isn't asynchronous - that is, it doesn't explicitly schedule
         * the execution of the CB - it is assumed that cbOnErr is checking the results
         * of an already asynchronous callback, i.e., that we're already in a scheduled
         * callback
         */
        cbOnErr: function(cb, potentialErr) {
            cb = cb || this.noop;
            if(potentialErr) {
                cb(potentialErr);
                return true;
            }
            return false;
        },

        doThrow: function(err) {
            throw err;
        },

        future: function(val) {
            return new SimpleFuture(val);
        },
        /**
         *
         * Given $lang.pojoKlass(["attr1", "attr2"]), returns equivalent of:
         *
         * var MyKlass = function() {
         *  this.initialize.apply(this, arguments);
         * };
         *
         * MyKlass.prototype = {
         *  initialize: function(attr1, attr2) {
         *      this._attr1 = attr1;
         *      this._attr2 = attr2;
         *  },
         *  attr1: function() {
         *      return this._attr1;
         *  },
         *  attr2: function() {
         *      return this._attr2;
         *  }
         * }
         */
        pojoKlass: function(attributes, opts) {
            attributes = attributes || [];
            opts = opts || {};
            var klass = function() {
                this.initialize.apply(this, arguments);
            };

            var klassInitializer = function() {
                var args = _.toArray(arguments);
                _.each(attributes, _.bind(function(v, k) {
                    var attr = "_" + v;
                    this[attr] = args[k];
                }, this));
            };

            klass.prototype.initialize = klassInitializer;

            _.each(attributes, function(attr) {
                var internalName = "_" + attr;
                var getter = function() {
                    return this[internalName];
                };
                klass.prototype[attr] = getter;
            });

            if (opts.setters) {
                _.each(attributes, function(attr) {
                    var internalName = "_" + attr;
                    var setterName = "set" + attr.charAt(0).toUpperCase() + attr.substr(1);
                    var setter = function(v) {
                        this[internalName] = v;
                    };
                    klass.prototype[setterName] = setter;
                });
            }



            return klass;
        }
    };

    var ScopedRequire = function() {
        this.initialize.apply(this, arguments);
    };

    _.extend(ScopedRequire.prototype, {
        initialize: function(target) {
            if (!target) {
                throw new Error("Invalid target provided for scoped require");
            }
            this._target = target;
        },
        req: function(attr, message) {
            if (!attr) {
                throw new Error("Invalid attribute provided");
            }
            message = message || "Attribute required but missing: `"+attr+"'";

            if (typeof this._target[attr] == "undefined" || this._target[attr] === null) {
                throw new Error(message);
            }
            return this;
        }
    });

    return new Lang();
};


if (typeof module !== "undefined" && module.exports) {
    module.exports = factory(
        require("underscore"),
        require("bit/commons/simple-future")
    );
} else if (typeof define !== "undefined") {
    define(["underscore", "bit/commons/simple-future"], factory);
}
