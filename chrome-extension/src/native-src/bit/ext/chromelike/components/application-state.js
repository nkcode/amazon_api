define([], function() {        
    var ApplicationStateManager  = function() {
        this.initialize.apply(this, arguments);
        this._states = {
                "initial": {
                    productComparisonEnabled : false,
                    alertsEnabled : false,
                    sessionTrackingEnabled : false,
                    showOemFirstrun : false,
                    showUpgradePrompt : false,
                    updateUBPRoot : false, // this is only for install, when extension selects locale as per config
                },
                "organicInstall": {
                    productComparisonEnabled : true,
                    alertsEnabled : true,
                    sessionTrackingEnabled : true,
                    showOemFirstrun : false,
                    showUpgradePrompt : false,
                    updateUBPRoot : true,
                },
                "oemInstall": {
                    productComparisonEnabled : false,
                    alertsEnabled : false,
                    sessionTrackingEnabled : false,
                    showOemFirstrun : true,
                    showUpgradePrompt : false,
                    updateUBPRoot : true,
                },
                "termsOfUsageAccepted": {
                    productComparisonEnabled : true,
                    alertsEnabled : true,
                    sessionTrackingEnabled : true,
                    showOemFirstrun : false,
                    showUpgradePrompt : false,
                    updateUBPRoot : false,
                },
                "termsOfUsageAcceptancePending": {
                    productComparisonEnabled : false,
                    alertsEnabled : false,
                    sessionTrackingEnabled : false,
                    showUpgradePrompt : false,
                    showOemFirstrun : true,
                    updateUBPRoot : false,
                },
                "stableState": {
                    productComparisonEnabled : true,
                    alertsEnabled : true,
                    sessionTrackingEnabled : true,
                    showUpgradePrompt : false,
                    showOemFirstrun : false,
                    updateUBPRoot : false,
                },
                "upgradeFromBookmark": {
                    productComparisonEnabled : true,
                    alertsEnabled : true,
                    sessionTrackingEnabled : true,
                    showUpgradePrompt : true,
                    showOemFirstrun : false,
                    updateUBPRoot : false,
                },
        }
    };

    ApplicationStateManager.prototype = {
        initialize : function() {
                         this._currentState  = "initial";
        },
        setCurrentState : function(state) {
                          this._currentState = state;
        },
        isProductComparisonEnabled : function() {
                          return this._states[this._currentState].productComparisonEnabled;
        },
        isAlertsEnabled : function() {
                          return this._states[this._currentState].alertsEnabled;
        },
        isSessionTrackingEnabled : function() {
                          return this._states[this._currentState].sessionTrackingEnabled;
        },
        shouldShowUpgradePrompt : function() {
                          return this._states[this._currentState].showUpgradePrompt;
        },
        shouldShowOemFirstrun : function() {
                          return this._states[this._currentState].showOemFirstrun;
        },
        shouldUpdateUBPRoot : function() {
                          return this._states[this._currentState].updateUBPRoot;
        },


    };

    return ApplicationStateManager;

});
