define([
    "underscore",
    "bit/commons/options",
    "bit/commons/flow",
    "bit/commons/task-manager",
    "bit/ext/chromelike/storage/native-storage",
    "bit/ext/core/storage/simple-storage",
    "bit/ext/core/components/token-vendor",
    "bit/ext/chromelike/platform/product-compass-frame",
    "bit/messaging/strategies/post-message-transport-strategy",
    "bit/ext/core/platform/platform-service",
    "bit/ext/core/components/campaign-attribution/ubp-feature-campaign-attribution",
    "bit/ext/core/platform/handlers/contextual/page-nav",
    "bit/ext/core/platform/handlers/contextual/content",
    "bit/ext/core/platform/handlers/contextual/sandbox",
    "bit/ext/core/platform/handlers/contextual/style",
    "bit/ext/core/platform/handlers/contextual/interaction",
    "bit/ext/core/platform/handlers/contextual/system",
    "bit/ext/core/platform/handlers/attribution/attribution",
    "bit/ext/core/platform/handlers/config/config",
    "bit/ext/core/platform/handlers/config/user-settings",
    "bit/ext/core/platform/scraper-peer-controller",
    "bit/ext/core/platform/sandbox-peer-controller",
    "bit/ext/core/platform/style-peer-controller",
    "bit/ext/core/platform/interaction-peer-controller",
    "bit/messaging/message-channel",
    "bit/messaging/message-exchange",
    "bit/ext/core/components/configuration-manager",
    "bit/ext/core/config/product-compass-local-config",
    "bit/commons/state-latch"
], function(
    _,
    $options,
    Flow,
    $taskManager,
    $nativeStorage,
    SimpleStorage,
    TokenVendor,
    ProductCompassFrame,
    PostMessageTransportStrategy,
    PlatformService,
    UBPCampaignAttrExports,
    PlatformPageNavHandler,
    PlatformContentHandler,
    PlatformSandboxHandler,
    PlatformStyleHandler,
    PlatformInteractionHandler,
    PlatformSystemHandler,
    PlatformAttributionHandler,
    PlatformConfigHandler,
    PlatformUserSettingsHandler,
    ScraperPeerController,
    SandboxPeerController,
    StylePeerController,
    InteractionPeerController,
    MessageChannel,
    MessageExchange,
    ConfigurationManager,
    ProductCompassConfig,
    StateLatch
) {

    "use strict";

    var ProductCompassManager = function() {
        this.initialize.apply(this, arguments);
    };

    var CHROME_SEND_MESSAGE = function(tabId, msg, cb) {
        chrome.tabs.sendMessage(tabId, msg, function(response) {
            if (!arguments.length) {
                cb(new Error("" + chrome.runtime.lastError));
            } else {
                cb(null, response);
            }
        });
    };
    var CHROME_EXECUTE_SCRIPT = function(tabId, scriptFile, cb) {
        chrome.tabs.executeScript(tabId, {
            file: scriptFile,
            allFrames: false,
            runAt: "document_idle"
        }, function(args) {
            cb();
        });
    };

    // The MessageExchange used by the PlatformService has a
    // 30 second time-out on sendAndReceive messages.
    // The health check period is greater than 30 seconds
    // because we don't want to have multiple terminateSession 
    // waiting for a reply.
    var CONSTANTS = {
        HEALTH_CHECK_PERIOD: 60 * 1000, //  1 minute
        HOST_PING_TIMEOUT: 30 * 1000    // 30 seconds
    };

    _.extend(ProductCompassManager.prototype, {
        initialize: function(opts) {
            opts = $options.fromObject(opts);

            this._platformEventMessageBus = opts.getOrError("platformEventMessageBus");
            this._platformSentinel = opts.getOrError("platformSentinel");


            // TODO: Set up of handlers doesn't belong in
            // the ProductCompassManager - these are PLATFORM handlers
            // Everything between BEGIN and END, along
            // with corresponding handlers, needs to be pulled
            // out and independently managed
            // BEGIN
            this._configMgr = new ConfigurationManager({
                config: ProductCompassConfig
            });

            this._userSettingsDelegate = opts.getOrError("userSettingsDelegate");
            this._attributionDelegate = opts.getOrError("attributionDelegate");
            this._localeDelegate = opts.getOrError("localeDelegate");
            // END


            this._productCompassFrame = null;
            this._transport = null;

            this._platformService = null;

            // Key/Value pairs of events/handlers
            this._eventHandlers = {};

            _.bindAll(this, "_healthCheck", "teardown");

            // Health check BEGINS
            // This should ideally be present in a separate module
            // that tracks state of all the applications.
            this._isActive = false;

            $taskManager.scheduleTask(this._healthCheck, CONSTANTS.HEALTH_CHECK_PERIOD);
            // Health check ENDS

            // _setupHandlers is bound using underscore.once so that it
            // only executes once even if it is invoked multiple times
            this._setupHandlers = _.once(_.bind(this._setupHandlers, this));

            // This semaphore ensures that only one call to the
            // asynchronous methods teardown and setup is active
            // at a time.
            this._productCompassManagerSemaphore = 1;

        },

        hostPing: function() {
            this._hostPingTimestamp = Date.now();
        },

        _healthCheck: function() {
            if (!this._isActive ||
                !this._hostPingTimestamp ||
                (Date.now() - this._hostPingTimestamp) > CONSTANTS.HOST_PING_TIMEOUT) {
                this.cycle();
            }
            $taskManager.scheduleTask(this._healthCheck, CONSTANTS.HEALTH_CHECK_PERIOD);
        },

        setup: function(options) {
            this._productCompassManagerSemaphore++;
            if (this._productCompassManagerSemaphore > 1) {
                this._productCompassManagerSemaphore = 1;
                return;
            }

            options = $options.fromObject(options);

            var setupParams = {};
            setupParams.locale = options.getOrElse('locale', 'xx');

            var setupLatch = new StateLatch([
                "knobCheck",
                "localeCheck",
                "settingsCheck"
            ], _.bind(function(err) {
                if (err) {
                    return;
                }
                this._productCompassFrame = new ProductCompassFrame({
                    endpoint: setupParams.endpoint,
                    params: {
                        inboundPort: 'window',
                        outboundPort: 'parent',
                        platform: 'Chrome'
                    },
                    locale: setupParams.locale
                });

                // Wire up ProductCompassFrame
                document.body.appendChild(this._productCompassFrame.getFrame());

                // _setupHandlers has been bound using underscore.once so that it
                // only executes once even if it is invoked multiple times
                this._setupHandlers();

                // Bind new frame to existing transport
                this._transport.bind({
                    inboundPort: window,
                    outboundPort: this._productCompassFrame.getPort()
                });

                // Add event handlers back to platformService
                _.each(this._eventHandlers, function(handler, ev) {
                    this._platformService.registry().register(ev, handler);
                }, this);

                this._isActive = true;
            }, this));

            var storage = new SimpleStorage($nativeStorage);
            storage.get("options.alertList", function(err, alertList) {
                // This handles the scenario where the alertList was not
                // initialized by the First Run page and is hence 'undefined'.
                if(!alertList) {
                    alertList = "sia";
                }                
                if (err || !_.contains(alertList.split(","), "sia")) {
                    setupLatch.error(err || new Error("ProductCompass is disabled in Settings."));
                    return;
                }
                setupLatch.trigger("settingsCheck");
            });

            this._configMgr.get("locales", function(err, locales) {
                // Locale check
                if (err || !locales) {
                    setupLatch.error(err || new Error("Error fetching locale details."));
                    return;
                }
                var localeConfig = locales[setupParams.locale];
                if (!localeConfig || !localeConfig.pcompHost || !localeConfig.knobValue || typeof localeConfig.knobValue.chrome === "undefined") {
                    setupLatch.error(new Error("Unsupported locale."));
                    storage.set("options.IsProductCompassEnabled", "false", function() {});
                    return;
                }

                // Add "sia" to the alertList for existing clients who haven't opened the gateway
                // since the silent update and hence don't have "sia" in the masterAlertList
                storage.get("options.previousAvailableAlerts", function(err, masterAlertList) {
                    if (err) {
                        return;
                    }
                    if (masterAlertList && !_.contains(masterAlertList.split(","), "sia")) {
                        var masterAlerts = masterAlertList.split(",");
                        if(masterAlerts.indexOf("nil") > -1) {
                            masterAlerts.splice(masterAlerts.indexOf("nil"), 1);
                        }
                        masterAlerts.push("sia");
                        masterAlertList = masterAlerts.join(",");
                        storage.set("options.previousAvailableAlerts", masterAlertList, function() {});                        
                        storage.get("options.alertList", function(err, alertList) {
                            if(err) {
                                return;
                            }
                            if(alertList && !_.contains(alertList.split(","), "sia")) {
                                var alerts = alertList.split(",");
                                if(alerts.indexOf("nil") > -1) {
                                    alerts.splice(alerts.indexOf("nil"), 1);
                                }
                                alerts.push("sia");
                                alertList = alerts.join(",");
                                storage.set("options.alertList", alertList, function() {});
                            }
                        });
                    }
                });

                setupLatch.trigger("localeCheck");
                // Get the endpoint.
                setupParams.endpoint = localeConfig.pcompHost;
                // Weblab check
                var tokenVendor = new TokenVendor({
                    storage: $nativeStorage
                });
                tokenVendor.verifyToken("ProductCompass", localeConfig.knobValue.chrome, function(err, isValid) {
                    if (err || !isValid) {
                        setupLatch.error(err || new Error("ProductCompass is inactive."));
                        storage.set("options.IsProductCompassEnabled", "false", function() {});
                        return;
                    }
                    setupLatch.trigger("knobCheck");
                    storage.set("options.IsProductCompassEnabled", "true", function() {});
                });
            });
        },

        // The callback 'cb' will always be invoked as cb() i.e.,
        // no errors or arguments will be passed to this callback.
        teardown: function(cb) {
            this._productCompassManagerSemaphore--;
            if (this._productCompassManagerSemaphore < 0) {
                this._productCompassManagerSemaphore = 0;
                return;
            }

            var tearDownLatch = new StateLatch([
                "terminateSessions"
            ], _.bind(function(err) {
                // Error or not, we teardown
                // Unregister events from platformService
                _.each(this._eventHandlers, function(handler, ev) {
                    this._platformService.registry().deregister(ev);
                }, this);

                // Unbind the transport from the frame and window
                if(this._transport) {
                    this._transport.unbind();
                }

                // Remove frame from DOM
                if(this._productCompassFrame) {
                    document.body.removeChild(this._productCompassFrame.getFrame());
                }
                this._productCompassFrame = null;
                this._isActive = false;
                cb();
            }, this));

            // This should use a separate exchange that has the semantics of
            // PlatformService being a client of PCompHost.
            try {
                if(!this._productCompassFrame) {
                    tearDownLatch.trigger("terminateSessions");
                    return;
                }                
                this._platformService.exchange().sendAndReceive({
                    mType: "platformRequest",
                    eventName: "Contextual.TerminateSessions"
                }, function(err) {
                    // Error or not, we tear down
                    if(err) {
                        tearDownLatch.error(err);
                        return;
                    }
                    tearDownLatch.trigger("terminateSessions");
                });
            } catch(err) {
                tearDownLatch.error(err);
            }
        },

        cycle: function() {
            Flow.getInstance().nextTick(this.teardown(_.bind(function() {
                this._localeDelegate.getLocale(_.bind(function(locale) {
                    this.setup({
                        locale: locale
                    });
                }, this));
            }, this)));
        },

        // _setupHandlers has been bound using underscore.once so that it
        // only executes once even if it is invoked multiple times
        _setupHandlers: function() {

            var PlatformEventMessage = {
                ContextualPageTurn: function(externalId, url, status) {
                    return {
                        mType: "platformEvent",
                        eventName: "Contextual.PageTurn",
                        args: {
                            externalId: externalId,
                            url: url,
                            // status should be "loading" or "complete"
                            status: status
                        }
                    };
                },
                ContextualExternalMessage: function(externalId, data) {
                    return {
                        mType: "platformEvent",
                        eventName: "Contextual.ExternalMessage",
                        args: {
                            externalId: externalId,
                            data: data
                        }
                    }
                }
            };

            this._transport = new PostMessageTransportStrategy({
                identity: "PlatformService"
            });

            this._platformService = new PlatformService({
                transportStrategy: this._transport
            });

            // BEGIN Contextual Page Turn
            var pageTurnEvent = "Contextual.PageTurn";
            var pageTurnHandler = new PlatformPageNavHandler({
                platformEventMessageBus: this._platformEventMessageBus
            });

            this._eventHandlers[pageTurnEvent] = pageTurnHandler;
            // END Contextual Page Turn


            // BEGIN Contextual Scraper
            var scraperPeerController = new ScraperPeerController({
                contentScripts: ["native-src/bit/ext/core/platform/contextual-libraries/scraper-library.js",
                                 "native-src/bit/ext/chromelike/platform/contextual-drivers/scraper-driver.js"],
                executeScript: CHROME_EXECUTE_SCRIPT,
                sendMessage: CHROME_SEND_MESSAGE
            });

            var scrapeContentEvent = "Contextual.ScrapeContent";
            var scrapeContentHandler = new PlatformContentHandler({peerController: scraperPeerController});

            this._eventHandlers[scrapeContentEvent] = scrapeContentHandler;
            // END Contextual Scraper


            // BEGIN Contextual Sandbox
            var sandboxPeerController = new SandboxPeerController({
                contentScripts: ["native-src/bit/ext/core/platform/contextual-libraries/sandbox-library.js",
                                 "native-src/bit/ext/chromelike/platform/contextual-drivers/sandbox-driver.js"],
                executeScript: CHROME_EXECUTE_SCRIPT,
                sendMessage: CHROME_SEND_MESSAGE
            });

            var sandboxHandler = new PlatformSandboxHandler({
                platformEventMessageBus: this._platformEventMessageBus,
                peerController: sandboxPeerController
            });

            this._eventHandlers["Contextual.CreateSandbox"] = sandboxHandler;
            this._eventHandlers["Contextual.ModifySandbox"] = sandboxHandler;
            this._eventHandlers["Contextual.DestroySandbox"]= sandboxHandler;
            // END Contextual Sandbox

            // BEGIN Contextual Style
            var stylePeerController = new StylePeerController({
                contentScripts: ["native-src/bit/ext/core/platform/contextual-libraries/style-library.js",
                                 "native-src/bit/ext/chromelike/platform/contextual-drivers/style-driver.js"],
                executeScript: CHROME_EXECUTE_SCRIPT,
                sendMessage: CHROME_SEND_MESSAGE
            });

            var styleHandler = new PlatformStyleHandler({
                platformEventMessageBus: this._platformEventMessageBus,
                peerController: stylePeerController
            });

            this._eventHandlers["Contextual.ApplyStyle"] = styleHandler;
            this._eventHandlers["Contextual.ResetStyle"] = styleHandler;
            // END Contextual Style

            // BEGIN Contextual Interaction
            var interactionPeerController = new InteractionPeerController({
                contentScripts: ["native-src/bit/ext/core/platform/contextual-libraries/interaction-library.js",
                                 "native-src/bit/ext/chromelike/platform/contextual-drivers/interaction-driver.js"],
                executeScript: CHROME_EXECUTE_SCRIPT,
                sendMessage: CHROME_SEND_MESSAGE
            });

            var interactionHandler = new PlatformInteractionHandler({
                platformEventMessageBus: this._platformEventMessageBus,
                peerController: interactionPeerController
            });

            this._eventHandlers["Contextual.RegisterPageBodyClick"] = interactionHandler;
            this._eventHandlers["Contextual.DeregisterMultipleEvents"] = interactionHandler;
            // END Contextual Interaction

            // BEGIN Contextual System
            var systemHandler = new PlatformSystemHandler({
                manager: this
            });

            this._eventHandlers["Contextual.HostPing"] = systemHandler;
            this._eventHandlers["Contextual.RestartHost"] = systemHandler;
            // END Contextual System

            //  BEGIN Attribution
            var attributionHandler = new PlatformAttributionHandler({
                delegate: this._attributionDelegate
            });

            this._eventHandlers["Attribution.GetRemoteAttributionParameters"] = attributionHandler;
            // END Attribution


            // BEGIN Configuration handler
            var configHandler = new PlatformConfigHandler({
                configurationManager: this._configMgr
            });
            this._eventHandlers["Config.Get"] = configHandler
            this._eventHandlers["Config.GetLocaleSpecificConfig"] = configHandler;
            // END Configuration handler

            // BEGIN User Settings handler
            this._eventHandlers["UserSettings.OpenSettings"] = new PlatformUserSettingsHandler({
                delegate: this._userSettingsDelegate
            });
            // END user settings handler


            chrome.tabs.onUpdated.addListener(_.bind(function(tabId, changeInfo, tab) {
                var msg = PlatformEventMessage.ContextualPageTurn(tabId, tab.url, changeInfo.status);
                this._platformEventMessageBus.publish(this._platformSentinel, msg);
            }, this));

            chrome.runtime.onMessage.addListener(_.bind(function(msg, sender, responseDelegate) {
                // We're going to drop the reponseDelegate for the purposes of this API.
                // Things sent along the bus are inherently 1-way, not request/reply

                // We're also going to drop messages that aren't from tabs. This event is
                // specifically for messages coming from tabs ("external content")

                // Lastly, we only care about mType "UBPExternalEvent", which we turn into
                // an "Contextual.ExternalEvent" internally.
                if (sender && sender.tab && msg && msg.mType === "UBPExternalMessage") {
                    this._platformEventMessageBus.publish(this._platformSentinel,
                        PlatformEventMessage.ContextualExternalMessage(sender.tab.id, msg.data)
                    );
                }
            }, this));

            chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
                if (changeInfo.status === "loading") {
                    scraperPeerController.resetState(tabId);
                    sandboxPeerController.resetState(tabId);
                    stylePeerController.resetState(tabId);
                    interactionPeerController.resetState(tabId);
                }
            });
        }
    });

    return ProductCompassManager;
});
