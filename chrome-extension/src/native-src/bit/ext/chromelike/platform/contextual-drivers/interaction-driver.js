(function() {

    "use strict";

    if (!window.UBPAPISupport.InteractionManager) {
        return;
    }

    var extensionMessageProxy = function(msg) {
        chrome.runtime.sendMessage(null, {
            mType: "UBPExternalMessage",
            data: msg
        });
    };

    window.UBPInteractionManagerInstance = new window.UBPAPISupport.InteractionManager({
        doc: window.document,
        onEvent: extensionMessageProxy
    });

    chrome.runtime.onMessage.addListener(function(msg, sender, cb) {
        if (msg.mType === "UBPInteractionRegisterPageBodyClick") {
            var handle = window.UBPInteractionManagerInstance.registerPageBodyClick();
            cb(handle);
        } else if (msg.mType === "UBPInteractionDeregisterMultipleEvents") {
            window.UBPInteractionManagerInstance.deregisterMultipleEvents({
                handle: msg.handle
            });
            cb();
        }
    });
}());