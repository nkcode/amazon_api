(function() {

    "use strict";

    if (!window.UBPAPISupport.StyleManager) {
        return;
    }

    window.UBPStyleManagerInstance = new window.UBPAPISupport.StyleManager(window.document);

    chrome.runtime.onMessage.addListener(function(msg, sender, cb) {
        if (msg.mType === "UBPStyleApplyStyle") {
            var handle = window.UBPStyleManagerInstance.applyStyle(msg.styleSpec);
            cb(handle);
        } else if (msg.mType === "UBPStyleResetStyle") {
            window.UBPStyleManagerInstance.resetStyle({
                handle: msg.handle
            });
            cb();
        }
    });
}());