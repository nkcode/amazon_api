(function() {
    if (!window.UBPAPISupport || !window.UBPAPISupport.SandboxLibrary) {
        return;
    }

    window.UBPSandboxManagerInstance = (function(){

        var SandboxManager         = window.UBPAPISupport.SandboxLibrary.SandboxManager,
            IFrameCreationStrategy = window.UBPAPISupport.SandboxLibrary.IFrameCreationStrategy;

        // Allows the SandboxManager to surface "external messages" to
        // extension.
        var extensionMessageProxy = function(msg) {
            chrome.runtime.sendMessage(null, {
                mType: "UBPExternalMessage",
                data: msg
            });
        };

        // The strategy encapsulates frame creation and modification
        var strategy = new IFrameCreationStrategy(document),
            // SoundManager encapsulates creation and handshake of sandboxes
            sandboxManager = new SandboxManager({
                strategy: strategy,
                onMessage: extensionMessageProxy,
                whitelistedOrigins: []
            });

        window.addEventListener('message', sandboxManager.sandboxMessageHandler);

        return sandboxManager;
    }());

    // Wire up the messages to the UBPSandboxManagerInstance methods
    chrome.runtime.onMessage.addListener(function(msg, sender, cb){
        // TODO: Remove
        console.log("Got message in page context", msg);
        if (msg.mType === "UBPSandboxCreateSandbox") {
            var sandboxSpec = msg.sandboxSpecification,
                handle = window.UBPSandboxManagerInstance.createSandbox(sandboxSpec);
            cb({handle: handle});
        } else if (msg.mType === "UBPSandboxAddWhitelistedOrigin") {
            var origin = msg.origin;
            window.UBPSandboxManagerInstance.addWhitelistedOrigin(origin);
            cb();
        } else if (msg.mType === "UBPSandboxModifySandbox") {
            var sandboxSpec = msg.sandboxSpecification,
                handle = msg.handle;

            window.UBPSandboxManagerInstance.modifySandbox(handle, sandboxSpec);
            cb();
        } else if (msg.mType === "UBPSandboxDestroySandbox") {
            var handle = msg.handle;
            window.UBPSandboxManagerInstance.destroySandbox(handle);
            cb();
        }
    });
}());
