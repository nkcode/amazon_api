(function() {
    if (!window.Scraper) {
        return;
    }
    chrome.runtime.onMessage.addListener(function(msg, sender, cb) {
        if (msg.mType === "UBPScraperScrape") {
            var spec = msg.scraperSpecification;
            var scraper = new Scraper();
            var result = scraper.scrape(document, spec);
            cb(result);
        }
    });
}());
