define([
    "underscore",
    "attribution-config",
    "bit/commons/options",
    "bit/commons/uri",
    "bit/ext/chromelike/config",
    "bit/ext/chromelike/components/update-event-interceptor",
    "bit/ext/chromelike/components/session-state-tracker",
    "bit/ext/core/components/attribution-manager",
    "bit/ext/chromelike/components/campaign-attribution-agent",
    "bit/commons/ready-gate",
    "bit/ext/core/util/ajax",
    "bit/ext/chromelike/storage/native-storage",
    "bit/ext/core/storage/simple-storage",
    "bit/ext/chromelike/components/application-state",
    "bit/commons/lang",
    "bit/commons/flow",
    // Special little snowflake
    "bit/messaging/message-channel",
    "bit/ext/chromelike/components/product-compass-manager",
    "bit/ext/core/components/configuration-manager",
    "bit/ext/core/config/main-local-config"
], function(
    _,
    $attributionConfig,
    $options,
    $uri,
    $config,
    $updateEventInterceptor,
    SessionStateTracker,
    AttributionManager,
    CampaignAttributionAgent,
    ReadyGate,
    $ajax,
    $nativeStorage,
    SimpleStorage,
    ApplicationStateManager,
    $lang,
    Flow,
    // Special little snowflake
    MessageChannel,
    ProductCompassManager,
    ConfigurationManager,
    MainLocalConfig
) {
    var debug = $config.isDebug();
    var root_locked = false;
    var SIX_HOURS = 21600000;
    var DEFAULT_TAGBASE = $attributionConfig.getTagbase();
    var CONSTANTS = {
        INSTALL_PING_RETRY_INTERVAL: 30 * 60 * 1000,        // 30 minutes
        DAILY_PING_RETRY_INTERVAL: 30 * 60 * 1000,          // 30 minutes
        FIRST_CLICK_PING_RETRY_INTERVAL: 30 * 60 * 1000     // 30 minutes
    };
    var storage = chrome.storage.local;
    var tagbase = DEFAULT_TAGBASE; // initializing tagbase with default value
    var thirdPartyId = localStorage.getItem("com.amazon.bit.chrome.thirdpartyid") || null;
    var panelPort;
    var cc_suffix_map = {
        'us' : '20',
        'ca' : '20',
        'uk' : '21',
        'gb' : '21',
        'fr' : '21',
        'de' : '21',
        'es' : '21',
        'it' : '21',
        'jp' : '22',
        'cn' : '23'
    };

    $simpleStorage = new SimpleStorage($nativeStorage);
    var applicationState = new ApplicationStateManager();

    var PLATFORM_SENTINEL = {};
    var PLATFORM_BUS = new MessageChannel({sentinel: PLATFORM_SENTINEL});
    var configMgr = new ConfigurationManager({
        config: MainLocalConfig
    });

    var getExtensionVersion = function() {
        var details = chrome.app.getDetails();
        return details.version;
    };

    var Logger = {
        log: function(message) {
            if (debug) {
                console.log(message);
            }
        }
    };

    Logger.log("Update event intercepter was triggered: " + $updateEventInterceptor.wasTriggered());
    Logger.log("Was updated: " + $updateEventInterceptor.wasUpdated() );
    Logger.log("Show upgrade prompt: " + $updateEventInterceptor.wasTriggered());

    // Every 6 hours the extension should send a ping
    // The first time ping runs it should also send an install ping
    var Activity = {
        getGUID: function(callback) {
            var s4 = function() {
              return Math.floor((1 + Math.random()) * 0x10000)
                         .toString(16)
                         .substring(1);
            };

            storage.get('guid', function(data) {
                var guid = data['guid'];
                if (!guid) {
                    guid = s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();

                    storage.set({'guid': guid}, function() {
                        callback(guid);
                    });

                }
                else {
                    callback(guid);
                }
            });
        },
        installPing: function() {
            Helper.getPingEndpoint(function(err, pingEndpoint) {
                if(err) {
                    setTimeout(Activity.installPing, CONSTANTS.INSTALL_PING_RETRY_INTERVAL);
                    return;
                }
                $ajax.get(pingEndpoint);
                Logger.log("Attempted to perform an install ping, endpoint used: " + pingEndpoint);
                storage.set({"options.install_ping": Date.now()});
            }, "/gp/ubp/misc/ping_pages/install_ping.html", true);
        },
        ping: function() {
            storage.get("options.last_ping", function(data) {
                var lastPingTime = data["options.last_ping"],
                    currentTime = Date.now();
                if (lastPingTime && (currentTime - lastPingTime < SIX_HOURS)) {
                    var timeLeft = SIX_HOURS - (currentTime - lastPingTime);
                    setTimeout(Activity.ping, timeLeft + 5000);
                    return;
                }
                storage.get('acceptedTermsOfUse', function(data) {
                    var endpoint = '/gp/ubp/misc/ping_pages/ping.html';

                    // in case it is not an oem install, data.acceptedTermsOfUse would be undefined
                    if(data.acceptedTermsOfUse === false) {
                        endpoint = '/gp/ubp/misc/ping_pages/installationIncomplete_ping.html';
                    }

                    Helper.getPingEndpoint(function(err, pingEndpoint) {
                        if(err) {
                            setTimeout(Activity.ping, CONSTANTS.DAILY_PING_RETRY_INTERVAL);
                            return;
                        }
                        $ajax.get(pingEndpoint);
                        Logger.log("Attempted to perform a daily ping, endpoint used: " + pingEndpoint);
                        storage.set({"options.last_ping": currentTime});
                        setTimeout(Activity.ping, SIX_HOURS + 5000);

                    }, endpoint);
                });
            });
        },
        firstClick: function() {
            Helper.getPingEndpoint(function(err, pingEndpoint) {
                if(err) {
                    setTimeout(Activity.firstClick, CONSTANTS.FIRST_CLICK_PING_RETRY_INTERVAL);
                    return;
                }
                $ajax.get(pingEndpoint);
                Logger.log("Attempted to perform first click ping, endpoint used: " + pingEndpoint);
                storage.set({"options.firstClick_ping": Date.now()});
            }, "/gp/ubp/misc/ping_pages/firstClick_ping.html");
        }
    };

    var HelperKlass = function() {
        this.initialize.apply(this, arguments);
    }

    _.extend(HelperKlass.prototype, {
        initialize: function() {

            var campaignAttrAgent = new CampaignAttributionAgent({
                endpoint: $config.getCampaignSatelliteEndpoint()
            });

            var tagbaseDelegate = function(cb) {
                cb = cb || $lang.noop;

                campaignAttrAgent.getTagbaseDelegate()(function(err, localStorageTagbase) {

                    if(err) {
                        cb(err);
                        return;
                    }

                    if(localStorageTagbase) {
                        Flow.getInstance().nextTick($lang.partiallyApply(cb, null, localStorageTagbase));
                    }
                    else {
                        // use the tagbase calculated locally in main.js
                        Flow.getInstance().nextTick($lang.partiallyApply(cb, null, tagbase));
                    }
                });
            };

            this._attributionManager = new AttributionManager({
                app: "1BA",
                platform: $attributionConfig.getAttributionPlatfom(),
                tagbaseDelegate: tagbaseDelegate,
                storage: $simpleStorage,
                campaignCodeDelegate: campaignAttrAgent.getCampaignCodeDelegate(),
                localeDelegate: this.getLocale
            });

            this._attributionManagerGate = new ReadyGate();
            this._attributionManagerGate.gated(_.bind(function(){
                this._attributionManager.start(this._attributionManagerGate.handler());
            }, this));

        },
        attributionManager: function() {
            return this._attributionManager;
        },
        getRemoteAttributionParameters: function(cb) {
            this._attributionManagerGate.onReady(_.bind(function() {
                this._attributionManager.getRemoteAttributionParameters(cb);
            }, this));
        },
        getAlertTypeFromAlertID: function(alertId) {
             /* Alert Types are 4 Char string - DOTD, PCOMP, SASS etc.
              * AlertID is an 8 char string:
              *     alertId - alertType (4 Chars) + random number(4 digits)
              *
              * Type = DOTD, alertId could be DOTD1245, DOTD9837 etc.
              * Type = SASS (Search Assist), alert Id could be SASS1882 etc
              */
              return  alertId.substring(0,4);
        },
        markAlertToasterAsShown: function(alertInternalId, cb) {
            // set toasterShown flag as true for the alert with given alertInternalId
            storage.get('options.alerts', function(options_alerts) {
                var allAlerts = options_alerts['options.alerts'];
                var type = Helper.getAlertTypeFromAlertID(alertInternalId);
                if(allAlerts && allAlerts[type]) {
                    for(var i=0;i<allAlerts[type].length;i++) {
                        if(allAlerts[type][i][alertInternalId] &&
                           !allAlerts[type][i][alertInternalId]['toasterShown']) {
                            allAlerts[type][i][alertInternalId]['toasterShown'] = 1;
                            break;
                        }
                    }
                    storage.set({'options.alerts': allAlerts}, function() {
                        cb();
                    });
                }
            });
        },
        clearSearchAssistAlert: function(alertId) {
            storage.get('options.alerts', function(options_alerts) {
                var alertType = Helper.getAlertTypeFromAlertID(alertId);
                var storedAlerts = options_alerts['options.alerts'][alertType];
                var i;
                for(i = 0;i < storedAlerts.length;i++) {
                    var notif = storedAlerts[i];
                    if(notif[alertId]) {
                        break;
                    }
                }
                storedAlerts.splice(i,1);
                options_alerts['options.alerts'][alertType] = storedAlerts;
                storage.set(options_alerts);
                // To ensure badge is correct, in case any unread identical alert was removed when this alert came
                Helper.updateBadge();
            });
        },
        findIdenticalAlertPreviouslyStored: function(newAlert, storedAlerts) {
            for(var i=0;i<storedAlerts.length; i++) {
                for(var key in storedAlerts[i]) {
                    if(storedAlerts[i].hasOwnProperty(key)){
                        // these properties would be different for any two alerts and do not require to match
                        var storedAlert = _.omit(storedAlerts[i][key], 'read', 'alertInternalId');
                        var matchAlert = _.omit(newAlert, 'read','alertInternalId');

                        if(_.isEqual(matchAlert, storedAlert) ){
                            return i;
                        }
                    }
                }
            }
            return -1;
        },
        toQueryString: function(params) {
            var key,
                pairs = [];
            for (key in params) {
                if (params.hasOwnProperty(key)) {
                    pairs.push(encodeURIComponent(key) + "=" + encodeURIComponent(params[key]));
                }
            }
            if (pairs.length === 0) {
                return "";
            } else {
                return pairs.join("&");
            }
        },
        _getParams: function(additionalParams) {

            var key,
            // Default params
            params = {
                version: getExtensionVersion(),
                now: Date.now()
            };

            if (thirdPartyId) {
                params["thirdpartyid"] = thirdPartyId;
            }

            for (key in additionalParams) {
                if (additionalParams.hasOwnProperty(key)) {
                    params[key] = (typeof additionalParams[key] === 'object') ? JSON.stringify(additionalParams[key]) : additionalParams[key];
                }
            }

            return params;
        },
        getRoot: function(callback, relative, additionalParams) {
            additionalParams = additionalParams || {};
            relative = relative || '';
            var self = this;
            Activity.getGUID(_.bind(function(guid){
                additionalParams = _.extend({}, additionalParams, {'guid': guid});
                this._attributionManagerGate.onReady(_.bind(function(err) {
                    if (err) {
                        Logger.log("Couldn't get attribution info " + err);
                        return;
                    }
                    this._attributionManager.getRefAttributionParameter(function(err, refParams) {
                        if (err) {
                            Logger.log("Couldn't get attribution info " + err);
                            return;
                        }
                        var finalParams = _.extend({}, additionalParams, refParams);
                        self._attributionManager.getRemoteAttributionParameters(function(err, params) {
                            finalParams = _.extend({}, finalParams, params);
                            storage.get('options.ubp_root', function(options_root) {
                                var root = options_root['options.ubp_root'] + relative +
                                "?" + self.toQueryString(self._getParams(finalParams));
                                Logger.log('Root url: ' + root);
                                callback(root);
                            });
                        });
                    });
                }, this));
            }, this));

        },
        getTaggedRoot: function(app, callback, relative, additionalParams) {
            additionalParams = additionalParams || {};
            relative = relative || '';
            var self = this;

            Activity.getGUID(_.bind(function(guid){
                additionalParams = _.extend({}, additionalParams, {'guid': guid});
                    this._attributionManagerGate.onReady(_.bind(function(err) {
                        if (err) {
                            Logger.log("Couldn't get attribution info " + err);
                            return;
                        }
                        this._attributionManager.getParams({
                            featureCode: app
                        }, function(err, params) {
                            var finalParams = _.extend({}, additionalParams, params);
                            storage.get('options.ubp_root', function(options_root) {
                                var root = options_root['options.ubp_root'] + relative +
                                "?" + self.toQueryString(self._getParams(finalParams));
                                Logger.log('Tagged url: ' + root);
                                callback(root);
                            });
                    });
                }, this));
            }, this));
        },
        getPingEndpoint: function(cb, relative, includeTag) {
            var self = this,
                params = {};

            // Get the Toolbar ID
            Activity.getGUID(function(guid) {
                params = _.extend({}, params, {
                    "guid": guid
                });

                // Check if AttributionManager is ready
                self._attributionManagerGate.onReady(function(err) {
                    if (err) {
                        cb(err);
                        return;
                    }

                    // Get the attribution parameters
                    self._attributionManager.getParams({
                        featureCode: "cfu"
                    }, function(err, attrParams) {
                        if (err) {
                            cb(err);
                            return;
                        }

                        // Collect all the parameters
                        params = _.extend({}, params, attrParams);

                        // Remove the tag if it is not asked for.
                        if (includeTag !== true) {
                            delete params.tag;
                        }

                        // Prepare the URL and invoke the callback
                        storage.get('options.ubp_root', function(data) {
                            var url = data['options.ubp_root'] + relative + "?"
                                      + self.toQueryString(self._getParams(params));
                            cb(null, url);
                        });
                    });
                });
            });
        },
        getGatewayUrl: function(callback, view) {
            var params = {};
            var app = "gw";
            if (view) {
                params["view"] = view;
                app = view === "firstrun" ? "fr" : "gw";
            }
            storage.get('options.gateway_page_url', function(options_gateway) {
                Helper.getTaggedRoot(app, function(root) {
                    Logger.log("Gateway URL: " + root);
                    callback(root);

                }, options_gateway["options.gateway_page_url"], params);
            });
        },
        // @param callback - function(locale)
        getLocale: function(callback) {
            var LOCALES = {
                "com"   : "Us",
                "ca"    : "Ca",
                "co.uk" : "Uk",
                "it"    : "It",
                "fr"    : "Fr",
                "es"    : "Es",
                "de"    : "De",
                "cn"    : "Cn",
                "co.jp" : "Jp",
                "jp"    : "Jp",
                "uk"    : "Uk"
            };
            storage.get('options.ubp_root', function(options_root) {
                configMgr.get("localeRegex", function(err, localeRegex) {
                    if(err) {
                        callback('Us');
                    }
                    var matches = $uri.parse(options_root['options.ubp_root']).host.match(new RegExp(localeRegex));
                    if(matches && matches[1] && LOCALES[matches[1]]) {
                        callback(LOCALES[matches[1]]);
                    }
                    else {
                        callback('Us');
                    }
                });
            });
        },
        getNotificationUrl: function(callback) {
            storage.get('options.notifications_page_url', function(options_notification) {
                Helper.getTaggedRoot("notif", function(root) {
                    Logger.log("Notification URL: " + root);
                    callback(root);
                }, options_notification['options.notifications_page_url']);
            });
        },
        createBackgroundPage: function() {
            Helper.getRoot(function(backgroundPageEndpoint) {
                var iframe = document.createElement('iframe');
                iframe.id = "UBPOneButtonBackgroundFrame";
                iframe.style.cssText = "margin: 0px; padding: 0px; border: 0px; border-style: none;";
                window.document.body.appendChild(iframe);
                iframe.src = backgroundPageEndpoint;
            }, '/gp/ubp/misc/background.html');
        },
        wishlistHelper: function(data) {
            Logger.log("Navigating to wishlist page");
            Logger.log(data);
            if(panelPort) {
                Helper.getTaggedRoot('ws',function(root) {
                    Logger.log("Wishlist_wishlistComplete relative root: "+root);
                    panelPort.postMessage({
                        UBPMessageType: "UBPMessageResponse",
                        type: "Application_autonavigate",
                        url: root,
                        postJSONData: data,
                        fullChromeApp: true
                    });
                },'/gp/ubp/oneButton/WL/addToWishlist');
            }
        },
        /**
         * Injects JavaScript code into a page. For details, see https://developer.chrome.com/extensions/content_scripts#pi.
         *
         * @param tabId The ID of the tab in which to run the script; defaults to the active tab of the current window.
         * @param details Details of the script to run.
         * @param Called after all the JavaScript has been executed.
         **/
        executeScript: function(tabId, details, callback) {
            /**
             * Make a best effort to work around a Chrome bug by reducing the chances that there are no Windows available.
             *
             * Chrome on some platforms has a bug that causes the extension process to crash if chrome.tabs.executeScript is called
             * with no specific Tab while there are no Windows available.  This can happen if Chrome processes are running in background (Windowless) mode.
             * 
             * See https://code.google.com/p/chromium/issues/detail?id=382923&thanks=382923&ts=1402409930 for more information.
             *
             * After the Chrome bug is fixed - we can consider removing the check for available Windows.
             */
            chrome.windows.getAll(null, function (windows) {
                if (!windows || !windows.length) {
                    Logger.log("Not injecting script because no windows exist");
                    return;
                }
                chrome.tabs.executeScript(tabId, details, callback);
            });
        },
        injectJS: function(options) {
            if (options) {
                var tabId = options.tabId || null; // inject JS to a specific Tab instead of default to the active one
                var execute = function(err, responseText) {
                    if (!err) {
                        Helper.executeScript(tabId, {code: responseText});
                    }
                };

                var self = this;
                if (options.includeShim) {
                    Logger.log('inject with message shim');
                    Helper.executeScript(tabId, {file: "ubpmessage.js"}, function() {
                        Helper.executeScript(tabId, {file: "injectedmessagelistener.js"}, function(args) {
                            if (args) {
                                (options.url) ? $ajax.get(options.url, execute) : execute(null, options.string);
                            } else {
                                Helper.handleShimError(options.shimErrorCallback);
                            }
                        });
                    });
                }
                else {
                    (options.url) ? $ajax.get(options.url, execute) : execute(null, options.string);
                }
            }
        },
        navigateToGateway : function () {
            if(panelPort) {
                Helper.getTaggedRoot('gw',function(root) {
                    panelPort.postMessage({
                        UBPMessageType: "UBPMessageResponse",
                        type: "Application_autonavigate",
                        url: root
                    });
                },'/gp/ubp/oneButton/gateway/render');
            }
        },
        handleShimError: function(shimCallback) {
            if (!shimCallback) {
                Helper.navigateToGateway();
            } else if (typeof shimCallback === "function") {
                shimCallback();
            } else {
                Logger.log("Unknown action");
            }

        },
        updateBadge: function() {
            /* Example alert storage structure:
             * { 'options.alerts' : {
             *                          'DOTD' : [{
             *                                      'DOTD1234' : {
             *                                                      'dealID' : 'abcd9876',
             *                                                      'title'  : '40% Off Skechers Shoes',
             *                                                      ...
             *                                                   }
             *                                    }],
             *                          'PCMP' : [{ 'PCMP3122' : {...}}, {'PCMP3245' : {...}}, ...],
             *                          'SASS' : [{ 'SASS2434' : {...}}, {'SASS6573' : {...}}, ...]
             *                      }
             * }
             */
            storage.get('options.alerts', function(options_alerts) {
                var count = 0;
                var allAlerts = options_alerts['options.alerts'];
                if(allAlerts) {
                    for(var type in allAlerts)  {
                        if(allAlerts.hasOwnProperty(type)) {
                            for(var i=0;i<allAlerts[type].length;i++) {
                                for(var prop in allAlerts[type][i]) {
                                    //Note that DOTD alerts are at times soft deleted so don't count them
                                    if(allAlerts[type][i].hasOwnProperty(prop) &&
                                        !allAlerts[type][i][prop]['read'] &&
                                        allAlerts[type][i][prop]['deleted'] !== "true") {
                                        count++;
                                    }
                                }
                            }
                        }
                    }
                }
                storage.set({'options.notification_count':count}, function(){
                    chrome.browserAction.setBadgeBackgroundColor({color:"#FF8000"});
                    chrome.browserAction.setBadgeText({text: count?count.toString():""});
                });
            });
        },
        /**
         * Mark all alerts as read, set the notification count to 0, and update the badge.
         */
        markAllAlertsAsRead: function() {
	        Logger.log("Marking all alerts as read");
	        storage.get('options.alerts', function(options_alerts) {
	            var allAlerts = options_alerts['options.alerts'];
	            if(allAlerts) {
	                for(var type in allAlerts)  {
	                    if(allAlerts.hasOwnProperty(type)) {
	                        for(var i=0;i<allAlerts[type].length;i++) {
	                            for(var prop in allAlerts[type][i]) {
	                                if(allAlerts[type][i].hasOwnProperty(prop))  {
	                                    allAlerts[type][i][prop]['read'] = 1;
	                                }
	                            }
	                        }
	                    }
	                }

	                // save the marked alerts, set the notifications count to 0 and update the badge
	                storage.set({'options.alerts': allAlerts}, function() {
						storage.set({'options.notification_count':0});
	                    Helper.updateBadge();
	                    response.value = true;
	                    port.postMessage(response);
	                });
	            }
	        });
        },
        loadPanelIframe: function(response, port) {
            if (applicationState.shouldShowUpgradePrompt()) {
                Helper.getTaggedRoot('gw', function(root) {
                    response.type = 'Application_reload';
                    response.url = root;
                    response.value = true;
                    port.postMessage(response);
                }, '/gp/ubp/misc/chromeupgraded.html');
            }
            else if (applicationState.shouldShowOemFirstrun()){
                var view = "firstrun";
                Helper.getGatewayUrl(function(root) {
                    response.type = 'Application_reload';
                    response.url = root;
                    response.value = true;
                    Logger.log('Send Application_reload');
                    port.postMessage(response);
                }, view);
            } else {
                // the default view is the gateway
                view = "gateway";

                // however, if there are any unread notifications, then display the notifications view
                storage.get('options.notification_count', function(options_count) {

                    if(options_count['options.notification_count']) {
                        /**
                         * Since avalone@ does not like ui for the notifications view,
                         * and since the only existing (6/25/2014) notification is the deal of the day notification,
                         * we show the deals view instead of the notifications view.
                         */
                        view = "deals";

                        // mark all alerts as read
                        Helper.markAllAlertsAsRead();
                    }
                    Helper.getGatewayUrl(function(root) {

                        response.type = 'Application_reload';
                        response.url = root;
                        response.value = true;
                        Logger.log('Send Application_reload');
                        port.postMessage(response);
                    }, view);
                });
            }
        }
    });

    var Helper = new HelperKlass();


    var MessageHandler = {
        handleMessage: function(message, port) {
            var response = {
                'id': message.id,
                'UBPMessageType': 'UBPMessageResponse'
            };

            switch (message.type) {
                case "Chrome_apptype":
                    response.type = 'Chrome_apptype';
                    storage.get('options.appList', function(options) {
                        var value = options['options.appList'];
                        Logger.log('applist!!' + value);
                        // If site bookmark is the only available app
                        // and they had the dumb button in the past open amazon directly
                        storage.get('hadNonOBExtension', function(hadNonOBExtension) {
                            if (value === 'visitfullsite' && hadNonOBExtension['hadNonOBExtension']) {
                                Helper.getTaggedRoot('logo', function(url) {
                                    response.value = 'bookmark_button';
                                    port.postMessage(response);
                                    chrome.tabs.create({ 'url': url });
                                });
                            }
                            else {
                                storage.get('options.default_gateway_width', function(width_options) {
                                    storage.get('options.default_gateway_height', function(height_options) {
                                        response.width = width_options['options.default_gateway_width'];
                                        response.height = height_options['options.default_gateway_height'];
                                        response.value = false;
                                        port.postMessage(response);
                                    });
                                });
                            }
                        });
                    });
                    break;

                case "Browser_crossDomainXHR":
                    if (!message.options || !message.options.method || !message.options.url) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        (function(port, response, message) {
                            var xhr = new XMLHttpRequest();
                            xhr.open(message.options.method, message.options.url, true);
                            xhr.onreadystatechange = function() {
                                if (xhr.readyState == 4) {
                                    response.value = xhr.responseText;
                                    port.postMessage(response);
                                }
                            }
                            if (message.options.method == "POST") {
                                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                xhr.setRequestHeader("Content-length", message.options.content.length);
                                xhr.setRequestHeader("Connection", "close");
                                xhr.send(message.options.content);
                            }
                            else {
                                xhr.send();
                            }
                        })(port, response, message);
                    }
                    break;

                case "Browser_openWindow":
                    if (!message.options || !message.options.url) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        port.postMessage({type: 'Application_hide'});
                        setTimeout(function() {
                        chrome.tabs.create({ 'url': message.options.url });
                        }, 500);
                    }
                    break;

                case "Util_getPageUrl":
                    chrome.tabs.getSelected(null, function(tab) {
                        response.value = tab ? tab.url : null;
                        port.postMessage(response);
                    });
                    break;

                case "Options_acceptTermsOfUse":
                    applicationState.setCurrentState("termsOfUsageAccepted");
                    storage.set({'acceptedTermsOfUse': true});
                    start();
                    break;

                case "Options_getOption":
                    if (!message.options || !message.options.name) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        var optionName = 'options.' + message.options.name;
                        Logger.log("Getting option " + optionName);
                        storage.get(optionName, function(options) {
                            response.value = options[optionName];
                            Logger.log("Get option value");
                            Logger.log(response.value);
                            port.postMessage(response);
                        });
                    }
                    break;

                case "Options_setOption":
                    if (!message.options || !message.options.name || !message.options.value) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        // We prefix the options with 'options.' as a key because of race conditions
                        // If we keep just an options key with a json as in the FF extension,
                        // sending two setOptions message will both asynchronously get the options and
                        // then overwrite each other on set.
                        // Ex: set foo to 1 and bar to 2.
                        // Get: {} in the foo set and get {} in the bar set
                        // Set { foo: 1 } followed by Set { bar: 2 }
                        // Next get of options will return { bar: 2 }, foo was overwritten
                        var optionName = 'options.' + message.options.name;
                        Logger.log("Setting option " + optionName + " with value " + message.options.value);
                        var dataObj = {};
                        if(message.options.name !== 'ubp_root' || root_locked == false) {
                            dataObj[optionName] = message.options.value;
                        }

                        storage.get(optionName, function(option){
                            var optionValue = option[optionName];
                            Logger.log("setting option for " +optionName + " " + message.options.value);
                            storage.set(dataObj, function() {
                                response.value = true;
                                port.postMessage(response);
                                if(message.options.name === 'ubp_root') {
                                    // Reload panel
                                    if (panelPort && port != panelPort) {
                                        Helper.getGatewayUrl(function(root) {
                                            response.type = 'Application_reload';
                                            response.url = root;
                                            panelPort.postMessage(response);
                                        });
                                    }

                                    //Reset notification counter
                                    storage.set({'options.notification_count':0}, function(){
                                        chrome.browserAction.setBadgeText({text:""});
                                    });
                                    //Clear all alerts
                                    storage.set({'options.alerts': {}});

                                }
                            });
                        });
                    }
                    break;

                case "Notifications_configure":
                    Logger.log("Sending backgroundPseudoPort message");

                    backgroundPseudoPort.postMessage({
                        type : "Background_reload"
                    });

                    // AmazonUBP.Notifications.configure({}) is called whenever they want to reload
                    // the Background page. We use this event to trigger ProductCompassManager setup.
                    // This addresses the problem of listening to every page that either sets or
                    // changes the alertList (some of which may be irrelevant) and only reloads
                    // the ProductCompassManager when necessary.
                    Helper.getLocale(function(locale) {
                        PLATFORM_BUS.publish(PLATFORM_SENTINEL, {
                            mType: 'Event.ResetBackgroundContext',
                            args: {
                                locale: locale
                            }
                        });
                    });
                    break;

                case "Notifications_setAlert":
                    if (!message.options || !message.options.type || !message.options.value) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {

                        Logger.log("Setting alert for " + message.options.type + " with value " + message.options.value);
                        //mark all alert as unread when storing. For a specific type alerts are stored in an array
                        for(var i=0;i<message.options.value.length;i++) {
                            for(var prop in message.options.value[i]) {
                                //the only prop in alert object is the alert id.
                                // along with 'read' flag which tells that the
                                // user has read/seen the alert, we also set 'toasterShown'
                                // flag, to know if we were really able to show
                                // the toaster for an alert or not.
                                message.options.value[i][prop]['read'] = 0;
                                message.options.value[i][prop]['toasterShown'] = 0;
                            }
                        }

                        storage.get('options.alerts', function(options_alerts){
                            var alertObj = {};
                            var storedAlerts = options_alerts['options.alerts'];
                            if(storedAlerts) {
                                storedAlerts[message.options.type] = message.options.value;
                                alertObj['options.alerts'] = storedAlerts;
                            }
                            else {
                                alertObj['options.alerts'] = {};
                                alertObj['options.alerts'][message.options.type] = message.options.value;
                            }
                            storage.set(alertObj, function(){
                                response.value = true;
                                port.postMessage(response);
                            });
                        });
                    }
                    break;

                case "Notifications_getAlert":
                    if (!message.options || !message.options.type) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        Logger.log("Getting alert for " + message.options.type );
                        storage.get('options.alerts', function(options_alerts){
                            var alerts = options_alerts['options.alerts'];
                            response.value = (alerts) ? alerts[message.options.type] : alerts;
                            port.postMessage(response);
                        });
                    }
                    break;

                case "Notifications_getAlerts":
                    if (!message.options) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        Logger.log("Getting all alerts " );
                        storage.get('options.alerts', function(options_alerts){
                            response.value = options_alerts['options.alerts'];
                            port.postMessage(response);
                        });
                    }
                    break;


                case "Notifications_clearAlert":
                    if (!message.options || !message.options.alertInternalId) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        Logger.log("Clearing Alert with internal id: " + message.options.alertInternalId);
                        var type = Helper.getAlertTypeFromAlertID(message.options.alertInternalId);
                        storage.get('options.alerts', function(options_alerts) {
                            var allAlerts = options_alerts['options.alerts'];
                            var removalIndex;
                            if(allAlerts && allAlerts[type]) {
                                for(var i=0;i<allAlerts[type].length;i++) {
                                    if(allAlerts[type][i][message.options.alertInternalId]) {
                                        removalIndex = i;
                                        break;
                                    }
                                }
                                if(!message.options.permanentdelete && type === "DOTD") {
                                    //soft delete only for deal of the day
                                    allAlerts[type][removalIndex][message.options.alertInternalId]['deleted'] = "true";
                                }
                                else {
                                    allAlerts[type].splice(removalIndex, 1);
                                    //if an array becomes empty remove that entire alert type from the storage
                                    if(allAlerts[type].length === 0) {
                                        delete allAlerts[type];
                                    }
                                }
                                storage.set({'options.alerts': allAlerts}, function() {
                                    Helper.updateBadge();
                                    response.value = true;
                                    port.postMessage(response);
                                });
                            }
                            else {
                                //not to block the execution of caller's success
                                //callback in case the alert object doesn't exist
                                response.value = true;
                                port.postMessage(response);
                            }
                        });
                    }
                    break;

                case "Notifications_clearSpecificTypeOfAlerts":
                    if (!message.options && !message.options.type) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        Logger.log("Clearing Alert for type: " + message.options.type);
                        storage.get('options.alerts', function(options_alerts) {
                            var allAlerts = options_alerts['options.alerts'];
                            var type = message.options.type;
                            if(allAlerts && allAlerts[type]) {
                                if(!message.options.permanentdelete && type === "DOTD") {
                                    //soft delete only for deal of the day
                                    for(var i=0;i<allAlerts[type].length;i++) {
                                        for(var prop in allAlerts[type][i]) {
                                            if(allAlerts[type][i].hasOwnProperty(prop))  {
                                                allAlerts[type][i][prop]['deleted'] = "true";
                                            }
                                        }
                                    }
                                }
                                else {
                                    // permanent delete
                                    delete allAlerts[type];
                                }
                                storage.set({'options.alerts': allAlerts}, function(){
                                    Helper.updateBadge();
                                    response.value = true;
                                    port.postMessage(response);
                                });
                            }
                            else {
                                //not to block the execution of caller's success
                                //callback in case the alert object doesn't exist
                                response.value = true;
                                port.postMessage(response);
                            }
                        });
                    }
                    break;

                case "Notifications_clearAllAlerts":
                    // all alerts have to be cleared
                    Logger.log("Removing all the alerts");
                    storage.get('options.alerts', function(options_alerts) {
                        var allAlerts = options_alerts['options.alerts'];
                        if(allAlerts) {
                            if(!message.options.permanentdelete) {
                                for(var type in allAlerts)  {
                                    if(allAlerts.hasOwnProperty(type)) {
                                        if(type === "DOTD") {
                                            //soft delete only for deal of the day
                                            for(var i=0;i<allAlerts[type].length;i++) {
                                                for(var prop in allAlerts[type][i]) {
                                                    if(allAlerts[type][i].hasOwnProperty(prop))  {
                                                        allAlerts[type][i][prop]['deleted'] = "true";
                                                    }
                                                }
                                            }
                                        }
                                        else {
                                            delete allAlerts[type];
                                        }
                                    }
                                }
                            }
                            else {
                                //permanently delete
                                allAlerts = {};
                            }
                            storage.set({'options.alerts': allAlerts}, function() {
                                Helper.updateBadge();
                                response.value = true;
                                port.postMessage(response);
                            });
                        }
                        else {
                            //not to block the execution of caller's success
                            //callback in case the alert object doesn't exist
                            response.value = true;
                            port.postMessage(response);
                        }
                    });
                    break;

                case "Notifications_markAlertAsRead":
                    if (!message.options || !message.options.alertInternalId) {
                        response.error = true;
                        if(port) {
                            port.postMessage(response);
                        }
                    }
                    else {
                        Logger.log("Marking alert as read with internal id: " + message.options.alertInternalId);
                        storage.get('options.alerts', function(options_alerts) {
                            var allAlerts = options_alerts['options.alerts'];
                            var type = Helper.getAlertTypeFromAlertID(message.options.alertInternalId);
                            if(allAlerts && allAlerts[type]) {
                                for(var i=0;i<allAlerts[type].length;i++) {
                                    if(allAlerts[type][i][message.options.alertInternalId] &&
                                       !allAlerts[type][i][message.options.alertInternalId]['read']) {
                                        allAlerts[type][i][message.options.alertInternalId]['read'] = 1;
                                        break;
                                    }
                                }
                                storage.set({'options.alerts': allAlerts}, function() {
                                    Helper.updateBadge();
                                    response.value = true;
                                    //checking port here bcoz this case is also called internally
                                    //from Alerts_toasterClicked, where port is passed as null
                                    if(port) {
                                        port.postMessage(response);
                                    }
                                });
                            }
                        });
                    }
                    break;

                case "Notifications_markSpecificTypeOfAlertsAsRead":
                    if (!message.options || !message.options.type) {
                        response.error = true;
                        port.postMessage(response);
                    }
                    else {
                        Logger.log("Marking Alerts as read for type: " + message.options.type);
                        storage.get('options.alerts', function(options_alerts) {
                            var allAlerts = options_alerts['options.alerts'];
                            var type = message.options.type;
                            if(allAlerts && allAlerts[type]) {
                                for(var i=0;i<allAlerts[type].length;i++) {
                                    for(var prop in allAlerts[type][i]) {
                                        if(allAlerts[type][i].hasOwnProperty(prop))  {
                                            allAlerts[type][i][prop]['read'] = 1;
                                        }
                                    }
                                }
                                //updateBadge when something was marked read
                                storage.set({'options.alerts': allAlerts}, function() {
                                    Helper.updateBadge();
                                    response.value = true;
                                    port.postMessage(response);
                                });
                            }
                        });
                    }
                    break;

                case "Notifications_markAllAlertsAsRead":
		            Helper.markAllAlertsAsRead();
                    break;

                case "Browser_injectJavaScriptFromURL":
                    Logger.log("Browser_injectJavaScriptFromURL: Injecting script from URL  " + message.options.jsurl);
                    Helper.injectJS({
                        tabId: (port && port.sender && port.sender.tab) ? port.sender.tab.id : null,
                        url: message.options.jsurl,
                        includeShim: message.options.includeShim,
                    });
                    response.value = true;
                    port.postMessage(response);
                    break;

                case "Browser_injectJavaScriptFromString":
                    Logger.log('inject ' + message.options.jsstring);
                    // Inject script into current tab
                    Helper.injectJS({
                        tabId: null, // TODO: fix me so the JS won't be injected into whichever tab is active
                        string: message.options.jsstring,
                        includeShim: message.options.includeShim,
                        shimErrorCallback: message.options.shimErrorCallback
                    });
                    response.value = true;
                    port.postMessage(response);
                    break;

                case "DebugLogs_log_message":
                    Logger.log('log ' + message.options.log);
                    response.value = true;
                    port.postMessage(response);
                    break;

                case "Panel_resizePanel":
                    Logger.log("Resizing mainPanel to " + message.options.width + "x" + message.options.height);
                    response.type = 'Application_resize';
                    response.width = message.options.width;
                    response.height = message.options.height;
                    response.value = true;
                    port.postMessage(response);
                    break;

                case "Panel_hidePanel":
                    Logger.log("Hiding mainPanel!");
                    port.postMessage({type: 'Application_hide'});
                    break;

                case "Chrome_upgradeContinue":
                    applicationState.setCurrentState("stableState");
                    Helper.getGatewayUrl(function(root) {
                        response.type = 'Application_reload';
                        response.url = root;
                        response.value = true;
                        port.postMessage(response);
                    });
                    break;

                case "Chrome_loadIframe":
                    Logger.log('Loading iframe');
                    storage.get('options.firstClick_ping', function(options_firstClick_ping) {
                        if(!options_firstClick_ping["options.firstClick_ping"]) {
                            Activity.firstClick();
                        }
                    });
                    Helper.loadPanelIframe(response, port);
                    break;

                case "Chrome_optionsload":
                    Helper.getTaggedRoot('set', function(first_run_url) {
                        first_run_url += '&embedded=true';
                        response.type = 'Options_reload';
                        response.url = first_run_url;
                        response.value = true;
                        port.postMessage(response);
                    }, '/gp/ubp/misc/settings/settings.html');
                    break;

                case "Chrome_hadNonOBExtension":
                    storage.get('hadNonOBExtension', function(hadNonOBExtension) {
                        if (hadNonOBExtension['hadNonOBExtension']) {
                            response.value = 'true';
                        }
                        else {
                            response.value = 'false';
                        }
                        port.postMessage(response);
                    });
                    break;
                case "Wishlist_wishlistComplete":
                    if (message.options) {
                        if (message.options.itemData) {
                            Helper.wishlistHelper(message.options.itemData);
                        } else { //sent with no data; either old WL or invalid page for scraping (action to take is the same either way)
                            Helper.navigateToGateway();
                        }
                    }
                    break;

                case "Alerts_toasterClicked":
                    if(message.options && message.options.alertInternalId) {
                        // If we are getting this message, that means toaster
                        // has been shown, so we can set the alert 'toasterShown' flag as true
                        Helper.markAlertToasterAsShown(message.options.alertInternalId, function() {
                            //mark the alert as read after the user has interacted & ignore in case of search assist that anyway is deleted
                            if(message.options.interacted && message.options.alertInternalId && message.options.appcode !== 'sa') {
                                MessageHandler.handleMessage({id: message.id, type:"Notifications_markAlertAsRead", options:{alertInternalId:message.options.alertInternalId}}, null);
                            }
                        });

                        if(message.options.interacted && message.options.appcode && !message.options.preventWebAppDisplay) {
                            Logger.log("Toaster clicked. Navigating to appcode=" + message.options.appcode);

                            if(message.options.appcode === "sa") {
                                Logger.log("Clear alert data for Search Assist");
                                Helper.clearSearchAssistAlert(message.options.alertInternalId);
                            }

                            storage.get('options.' + message.options.appcode + '_page_url', function(options_appUrl){
                                storage.get('options.ubp_root', function(options_root){
                                    storage.get('options.' + message.options.appcode + '_app_width', function(options_appWidth){
                                        storage.get('options.' + message.options.appcode + '_app_height', function(options_appHeight){
                                            Helper.attributionManager().getParams({}, function(err, attrParams) {
                                                var finalParams = _.extend({}, attrParams);
                                                var appUrl = options_root['options.ubp_root'] + options_appUrl['options.' + message.options.appcode + '_page_url'] + "?" + 'version=' + encodeURIComponent(getExtensionVersion());
                                                if (message.options.queryParams) {
                                                    try {
                                                        // toQueryString will encode every query params to avoid XSS attack
                                                        _.extend(finalParams, message.options);
                                                    } catch (e) {
                                                        Logger.log('Alerts_toasterClicked: error parsing queryParams');
                                                    }

                                                }

                                                if (appUrl.indexOf("tagbase=") === -1) {
                                                    finalParams["tagbase"] = tagbase;
                                                }

                                                appUrl += "&" + Helper.toQueryString(finalParams);

                                                var appWidth = options_appWidth['options.' + message.options.appcode + '_app_width'];
                                                var appHeight = options_appHeight['options.' + message.options.appcode + '_app_height'];

                                                Logger.log("appWidth and appHeight: " + appWidth + "x" + appHeight);
                                                Logger.log("appUrl to be shown in web view: " + appUrl);
                                                //This JS contains the message handling for cross
                                                //button. Eventually for complete web-view experience,
                                                //it will move to a separate file, leaving just the
                                                //iframe div creation here
                                                var jsForWebview = '(function(window,document){var app_gateway = document.createElement("div");var ifrm = document.createElement("IFRAME");ifrm.src = "' + appUrl + '";app_gateway.style.width = "' + appWidth + 'px";ifrm.id = "AmazonNotificationFrame"; app_gateway.id= "AmazonNotificationOuterDiv"; app_gateway.style.height = "' + appHeight + 'px";app_gateway.style.position = "fixed";ifrm.style.width = "' + appWidth + 'px";ifrm.style.height = "' + appHeight + 'px";ifrm.style.position = "relative";ifrm.style.borderWidth = "0";ifrm.style.margin = "0";ifrm.style.padding = "0";ifrm.style.borderStyle = "none";app_gateway.style.right = "5px";app_gateway.style.top = "2px";app_gateway.style.zIndex = "2147483647";app_gateway.style.background = "#fff";app_gateway.style.color = "#333";app_gateway.style.margin = "8px 0";app_gateway.style.border = "1px solid #999";app_gateway.appendChild(ifrm);document.body.appendChild(app_gateway);document.body.onmousedown = function(e) {elem = document.getElementById("AmazonNotificationOuterDiv"); if(elem) {elem.parentNode.removeChild(elem);}}; var amznUBPOneButtonWebApp = new Object(); function receiveMessage(e) { if (e.data.UBPMessageType === "UBPMessage") {var app_frame = document.getElementById("AmazonNotificationOuterDiv"); if (app_frame && e.data.type==="Panel_hidePanel") { var customEvent = new Object(); customEvent.target = app_frame; amznUBPOneButtonWebApp.load(customEvent); }}} window.addEventListener("message", receiveMessage, false); amznUBPOneButtonWebApp.load = function (mainEvent) { var app_frame = mainEvent.target; if (app_frame) { app_frame.parentNode.removeChild(app_frame); } return true; };}(window,document));';
                                                MessageHandler.handleMessage({id: message.id, type:"Browser_injectJavaScriptFromString", options:{jsstring:jsForWebview, includeShim: true}}, port);
                                            });

                                        });
                                    });
                                });
                            });
                        }
                        else {
                            Logger.log("Toaster not clicked. Update counter.");
                            Helper.updateBadge();
                        }
                    }
                    break;

                case "Core_getVersion":
                    Logger.log("Getting extension version");
                    response.value = getExtensionVersion();
                    port.postMessage(response);
                    break;

                case "Core_getToolbarId":
                    Logger.log("Getting toolbar id");
                    var guid = Activity.getGUID(function(guid){
                        response.value = guid;
                        port.postMessage(response);
                    });
                    break;

                default:
                    // Unknown message
                    response.error = true;
                    port.postMessage(response);
                    break;
            }
        }
    }

    var InitializeDefaultOverrides = function() {
        Logger.log('InitializeDefaultOverrides');
        var skipPersist = {
            "invalidTagbase": 1,
            "overrideTagbase": 1
        };
        Helper.getRoot(function(root) {
            Logger.log('InitializeDefaultOverrides ubp_root: ' + root);
            $ajax.getJson(root, function(err, oneButtonConfig) {
                if (err) return;
                for(var key in oneButtonConfig){
                    if (skipPersist[key] || (key === 'ubp_root' && !applicationState.shouldUpdateUBPRoot() && root_locked == false) || (key === 'gateway_page_url' && applicationState.shouldShowOemFirstrun())) {
                        continue;
                    } else {
                        var optionName = 'options.' + key
                        var dataObj = {};
                        dataObj[optionName] = oneButtonConfig[key];
                        Logger.log('Setting:');
                        Logger.log(dataObj);
                        //in case of installation, we need to send install ping
                        //after the root is set
                        if(key === 'ubp_root' && $updateEventInterceptor.wasInstalled()) {
                            storage.set(dataObj, function(){
                                Activity.installPing();
                            });
                        }
                        else {
                            storage.set(dataObj);
                        }
                    }
                }

                if(oneButtonConfig['invalidTagbase'] && oneButtonConfig["overrideTagbase"]) {
                    tagbase = oneButtonConfig["overrideTagbase"];
                    // XXX: This isn't safe - #set is async. Potential race condition.
                    storage.set({'tagbase': tagbase });
                }

                // Use this for testing
                // if(applicationState.shouldUpdateUBPRoot()) {
                //     storage.set({'options.ubp_root': $config.getDefaultRoot() });
                // }

                if(!applicationState.shouldShowOemFirstrun()) {
                    Helper.createBackgroundPage();
                    startSessionTracker();
                }
                if(applicationState.shouldUpdateUBPRoot() && !applicationState.shouldShowOemFirstrun()) {
                    Helper.getTaggedRoot('fr', function(first_run_url) {
                        chrome.tabs.create({ 'url' : first_run_url });
                    }, '/gp/ubp/misc/firstrunpage.html');
                }

                // Attempt to redeem promotion if any
                Helper.attributionManager().startRedemption(function(err){
                    if (err) {
                        Logger.log("Couldn't redeem promotion. Error: " + err.message);
                        return;
                    }
                });

                Logger.log("Starting ping");
                Activity.ping();
            });
        }, "/gp/ubp/json/config/setup");
    };

    if (debug) {
        storage.clear();
    }


var start = function () {
    storage.get('options.ubp_root', function(options) {
        Logger.log(options['options.ubp_root']);
        // in case of non-oem install, isOemInstall and acceptedTermsOfUse would be undefined
        storage.get('isOemInstall', function(isOemInstall) {
            storage.get('acceptedTermsOfUse', function(acceptedTermsOfUse) {
                if (!options['options.ubp_root']) {
                    storage.set({'options.ubp_root': $config.getDefaultRoot() }, function() {
                        storage.set({'options.gateway_page_url': '/gp/ubppf/gateway'}, function() {
                        storage.set({'options.default_gateway_width': 394}, function() {
                        storage.set({'options.default_gateway_height': 566}, function() {

                        if(isOemInstall['isOemInstall'] && !acceptedTermsOfUse['acceptedTermsOfUse']) {
                            applicationState.setCurrentState("oemInstall");
                            storage.set({'options.gateway_page_url': '/gp/ubppf/firstrun'});
                        } else {
                            $updateEventInterceptor.shouldShowUpgradePrompt() ? applicationState.setCurrentState("upgradeFromBookmark") : applicationState.setCurrentState("organicInstall");
                        }
                        InitializeDefaultOverrides();
                        });
                        });
                        });
                    });
                 } else {
                     if(isOemInstall['isOemInstall'] && !acceptedTermsOfUse['acceptedTermsOfUse']) {
                         applicationState.setCurrentState("termsOfUsageAcceptancePending");
                     } else {
                         applicationState.setCurrentState("stableState");
                     }
                     Logger.log('Extension already set up');
                     InitializeDefaultOverrides();
                }
            });
        });
    });
};
    storage.get('tagbase', function(tagbaseOption) {
        // Only set here if there was actually one stored - that way
        // it continues to default to abba-chrome rather than falling
        // through to the catch-all/overrideTagbase received in the setup
        // config
        tagbase = tagbaseOption['tagbase'] || tagbase;
        start();
    });

    chrome.extension.onConnect.addListener(function(port) {
        if (!port){
            return;
        }
        Logger.log(port.name);
        if (port.name === 'UBPAppsChromePanel') {
            panelPort = port;
            panelPort.onDisconnect.addListener(function() {
                panelPort = false;
            });
        }
        port.onMessage.addListener(function(message) {
            Logger.log('Extension got message');
            Logger.log(message);
            MessageHandler.handleMessage(message, port);
        });
    });

    var backgroundPseudoPort = {

        postMessage: function(response) {

            if (response.type == 'Background_reload') {
                Helper.getRoot(function(backgroundPageEndpoint) {
                    var backgroundFrameElement = document.getElementById('UBPOneButtonBackgroundFrame');
                    if(backgroundFrameElement) {
                        backgroundFrameElement.src = backgroundPageEndpoint;
                    }
                }, '/gp/ubp/misc/background.html');
                delete response.type;
            }
            if(response.id || response.type === 'Configure_notifications') {
                // TODO: We should always know the targetOrigin here... specify it as such.
                document.getElementById('UBPOneButtonBackgroundFrame').contentWindow.postMessage(response, '*');
            } else if (response.type === 'SessionTracking_relayCRSChange') {
                // TODO: We should always know the targetOrigin here... specify it as such.
                document.getElementById('UBPOneButtonBackgroundFrame').contentWindow.postMessage(response, '*');
            }
        }

    };

    var receiveMessage = function(message) {
        delete message.error;
        delete message.success;
        MessageHandler.handleMessage(message.data, backgroundPseudoPort);
    };
    window.addEventListener('message', receiveMessage, false);

    var startSessionTracker = function() {
        // var sessionTracker = new SessionStateTracker({
        //     port: backgroundPseudoPort,
        //     helper: Helper
        // });
        // chrome.tabs.onUpdated.addListener(sessionTracker.onPageUpdated);
    };

    // =============
    // BEGIN UBPv1.5
    // =============

    (function() {
        // Manages the ProductCompassFrame and its Messaging mechanism
        var productCompassManager = new ProductCompassManager({
            platformEventMessageBus: PLATFORM_BUS,
            platformSentinel: PLATFORM_SENTINEL,
            // Helper is used as a localeDelegate since it provides the getLocale()
            // method that is used by ProductCompassManager to fetch the current locale.
            localeDelegate: Helper,
            // Helper's getRemoteAttributionParameters takes care of ensuring that
            // setup has occurred before the actual call to the underlying
            // AttributionManager's getRemoteAttributionParameters is called, thus
            // we use the Helper as the delegate, rather than the manager itself.
            // Hopefully this goes away when we re-write the Helper stuff...
            attributionDelegate: Helper,
            userSettingsDelegate: {
                openSettings: function(args, cb) {
                    Helper.getTaggedRoot('set', function(url) {
                        url += '&embedded=true';
                        chrome.tabs.create({ 'url' : url });
                        cb();
                    }, '/gp/ubp/misc/settings/settings.html')
                }
            }
        });

        // Teardown and setup again when background page is reset.
        // The intent here is to recycle the ProductCompassManager
        // when either the locale changes or user preferences change.
        // However, since these events may happen at the same time,
        // there might be a race condition that causes 2 cycle invocation
        // in quick succession, which is not desirable.
        // The handshaking that maseb@ is working on will help resolve this
        // in an ideal fashion.
        PLATFORM_BUS.subscribe(function(message) {
            if (message.mType === 'Event.ResetBackgroundContext') {
                productCompassManager.cycle();
            }
        });

        window.productCompassManager = productCompassManager;

    }());

    // ============
    // END UBPv1.5
    // ============
});

