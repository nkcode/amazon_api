var factory = function() {

    "use strict";

    /**
     * A component that scrapes Content from an HTMLDocument according the rules
     * dictated by a ScraperSpecification.
     *
     * A Scraper is used in the following manner:
     *
     * <code>
     * // create a scraper
     * Scraper scraper = new Scraper();
     *
     * // create the specification that describes how to scrape content
     *  var specification = {
     *      contentType : "Price", // scrape price Content
     *      scraperType : "Xpath", // scrape using an xpath expression
     *      scraper : "//div[@id='WM_PRICE']/div/div[1]/span", // scrape using this xpath selector
     *      constraint : { // impose additional constraints on the scraped content
     *          constraintType: "JsRegex",
     *          regexPattern: "\\$[0-9]+\\.[0-9]{2}",
     *          isDefined: true
     *      }
     *  };
     *
     *  // perform the scraping - notice that the HTMLDocument is specified as the first parameter
     *  var content = scraper.scrape(window.document, specification);
     *
     *  // inspect the results
     *  console.log(content.contentType); // this will log "Price"
     *  console.log(content.contentBody); // this will log the price scraped from the HTMLDocument
     * </code>
     */
    var Scraper = function() {
        this.initialize.apply(this, arguments);
    };
    Scraper.prototype = {

        /**
         * Initializes this Scraper.
         */
        initialize: function() {

            // install specification expression evaluators
            // one per specification expression type
            this.evaluators = {};
            this.evaluators["UrlJsRegex"] = evaluateUrlJsRegex;
            this.evaluators["Xpath"] = evaluateXpath;
            this.evaluators["Css"] = evaluateCss;

            // install content constraint checkers
            // one per content constraint type
            this.constraintCheckers = {};
            this.constraintCheckers["None"] = noneConstraintChecker;
            this.constraintCheckers["JsRegex"] = jsRegexConstraintChecker;
        },

        /**
         * Attempts to scrape Content from the specified HTMLDocument according
         * to the specified ScraperSpecification.
         *
         * @param htmlDocument
         *            The HTMLDocument to scrape Content from. See
         *            https://developer.mozilla.org/en-US/docs/Web/API/HTMLDocument
         *            for more information about HTMLDocuments.
         * @param specification
         *            The ScraperSpecification that specifies how to scrape
         *            Content from the specified HTMLDocument. See
         *            bit.pcomp.scrape.ScraperSpecification in
         *            BITProductCompassCore for more information about
         *            ScraperSpecifications.
         *
         * @return A Content if one could be scraped according to the specified
         *         ScraperSpecification. If no appropriate Content could be
         *         found, then null is returned.
         */
        scrape: function(htmlDocument, specification) {
            if (!htmlDocument) {
                throw new Error("htmlDocument must be specified: " + htmlDocument);
            }
            if (!specification) {
                throw new Error("specification must be specified: " + specification);
            }
            if (!specification.contentType) {
                throw new Error("specification must have a content type");
            }
            if (!specification.scraperType) {
                throw new Error("specification must have an scraper type");
            }
            if (!specification.scraper) {
                throw new Error("specification must have an scraper expression");
            }
            // select the appropriate expression evaluator
            var evaluator = this.evaluators[specification.scraperType];
            if (!evaluator) {
                throw new Error("No evaluator installed for scraper type: " + specification.scraperType);
            }

            // evaluate the expression
            var contentValue = evaluator(htmlDocument, specification.scraper);

            // if no content was found, return null
            if (!contentValue) {
                return null;
            }

            var content = new Content(specification.contentType, contentValue);

            // check the content constraint
            if (specification.constraint && specification.constraint.isDefined) {
                var constraintChecker = this.constraintCheckers[specification.constraint.constraintType];
                if (!constraintChecker) {
                    // invalid content constraint type
                    throw new Error("No constraint checker installed for constraint type: " + specification.constraint.type);
                } else if (!constraintChecker(specification.constraint, content)) {
                    // content did not satisfy the constraint
                    return null;
                }
            }

            // return the scraped content
            return content;
        }
    };

    /***************************************************************************
     * Expression Evaluators:
     **************************************************************************/

    /**
     * Evaluates the UrlJsRegex scraper expression against the URL of the
     * specified HTMLDocument. A Content object is returned if non-trivial
     * Content matching the ScraperSpecification could be found; null is
     * returned otherwise.
     *
     *
     * A UrlJsRegex expression is composed of two components, one per line:
     *
     * <code>
     *     MatchPattern\n
     *     ContentPattern
     * </code>
     *
     * The MatchPattern must match the URL for Content to be scraped. The
     * ContentPattern is a substitution pattern for the URL. The ContentPattern
     * may use numbered backreferences to the groupings in the MatchPattern
     * result.
     *
     * This method is called by Scraper#scrape. Note that content constraints
     * are not checked by this method. They are checked by Scraper#scrape.
     *
     * @param htmlDocument
     *            The HTMLDocument to attempt to scrape Content from (the
     *            HTMLDocument's URL will be scraped). See https://
     *            developer.mozilla.org/en-US/docs/Web/API/HTMLDocument for more
     *            information about HTMLDocuments.
     * @param expression
     *            The UrlJsRegex expression that describes how to scrape
     *            Content.
     *
     * @return A Content object if non-trivial Content matching the
     *         ScraperSpecification could be found; null otherwise.
     */
    var evaluateUrlJsRegex = function(htmlDocument, expression) {

        // parse the patterns out of the expression
        var patterns = expression.split("\n");
        if (patterns.length !== 2) {
            throw new Error("UrlJsRegex expression must have exactly two lines: " + patterns);

        }
        var matchPattern = patterns[0];
        var contentPattern = patterns[1];
        var regex = new RegExp(matchPattern);

        // retrieve the url
        var url = htmlDocument.URL;

        // ensure the regex matches the url
        var matches = url.match(regex);
        if (!matches || !matches[0]) {
            return null;
        }

        // compute the content value
        var contentValue = url.replace(regex, contentPattern);

        // don't return empty content
        if (!contentValue) {
            return null;
        }

        // return the content value
        return contentValue;
    };

    /**
     * Evaluates the Xpath scraper expression against the specified
     * HTMLDocument. A Content object is returned if non-trivial Content
     * matching the ScraperSpecification could be found; null is returned
     * otherwise.
     *
     * A Xpath scraper expression is an XPath selector. See
     * https://developer.mozilla.org/en-US/docs/Introduction_to_using_XPath_in_JavaScript
     * for more information about XPath.
     *
     * This method is called by Scraper#scrape. Note that content constraints
     * are not checked by this method. They are checked by Scraper#scrape.
     *
     * @param htmlDocument
     *            The HTMLDocument to attempt to scrape Content from (the
     *            HTMLDocument's URL will be scraped). See https://
     *            developer.mozilla.org/en-US/docs/Web/API/HTMLDocument for more
     *            information about HTMLDocuments.
     * @param expression
     *            The Xpath expression that describes how to scrape
     *            Content.
     *
     * @return A Content object if non-trivial Content matching the
     *         ScraperSpecification could be found; null otherwise.
     */
    var evaluateXpath = function(htmlDocument, expression) {

        // https://developer.mozilla.org/en-US/docs/Web/API/document.evaluate
        var result = htmlDocument.evaluate(expression, htmlDocument, null, 9, null);

        // retrieve the node
        var node = result.singleNodeValue;
        if (!node) {
            return null;
        }

        // retrieve the inner text of the selected node
        var contentValue = node.innerText;
        if (!contentValue) {
            return null;
        }

        // return the inner text as the content value
        return contentValue;
    };

    /**
     * Evaluates the Css scraper expression against the specified HTMLDocument.
     * A Content object is returned if non-trivial Content matching the
     * ScraperSpecification could be found; null is returned otherwise.
     *
     * A Css scraper expression is a CSS Selector. See
     * https://developer.mozilla.org/en-US/docs/Web/API/document.querySelector
     * for more information about CSS Selectors.
     *
     * This method is called by Scraper#scrape. Note that content constraints
     * are not checked by this method. They are checked by Scraper#scrape.
     *
     * @param htmlDocument
     *            The HTMLDocument to attempt to scrape Content from (the
     *            HTMLDocument's URL will be scraped). See https://
     *            developer.mozilla.org/en-US/docs/Web/API/HTMLDocument for more
     *            information about HTMLDocuments.
     * @param expression
     *            The CSS expression that describes how to scrape
     *            Content.
     *
     * @return A Content object if non-trivial Content matching the
     *         ScraperSpecification could be found; null otherwise.
     */
    var evaluateCss = function(htmlDocument, expression) {

        // https://developer.mozilla.org/en-US/docs/Web/API/document.querySelector
        var result = htmlDocument.querySelector(expression);
        if (result === null) {
            return null;
        }

        // retrieve the inner text of the selected node
        var contentValue = result.innerText;
        if (!contentValue) {
            return null;
        }

        // return the inner text as the content value
        return contentValue;
    };

    /***************************************************************************
     * Constraint Checkers:
     **************************************************************************/

    /**
     * The trivial constraint checker function that is satisfied by all Content.
     */
    var noneConstraintChecker = function(constraint, content) {
        return true;
    };

    /**
     * A constraint checker function that requires that the Content's value
     * matches the JavaScript regex in the value of the specified constraint.
     */
    var jsRegexConstraintChecker = function(constraint, content) {
        var regex = new RegExp(constraint.regexPattern);
        if (!content.contentBody.match(regex)) {
            return false;
        }
        return true;
    };

    /**
     * Represents Content scraped from an HTMLDocument.
     */
    var Content = function() {
        this.initialize.apply(this, arguments);
    };

    Content.prototype = {

        /**
         * Initializes this Content with the specified content type and string
         * value.
         *
         * See bit.pcomp.concepts.ContentType for the full enumeration of
         * possible ContentTypes.
         */
        initialize: function(contentType, value) {
            if (!contentType) {
                throw new Error("contentType must be specified: " + contentType);
            }
            if (!value) {
                throw new Error("value must be specified: " + value);
            }
            this.contentType = contentType;
            this.contentBody = value;
        }
    };

    return Scraper;
};
if (typeof module !== "undefined" && module.exports) {
    module.exports = factory();
} else if (typeof define !== "undefined") {
    define([], factory);
}

if (typeof window !== "undefined") {
    window.Scraper = factory();
}