var factory = function(
    _,
    $options,
    MessageExchange,
    MessageDispatcher,
    PlatformAPIRegistry
) {
    var PlatformService = function() {
        this.initialize.apply(this, arguments);
    };

    _.extend(PlatformService.prototype, {
        initialize: function(opts) {
            opts = $options.fromObject(opts);

            var transportStrategy = opts.getOrError("transportStrategy");

            this._exchange = new MessageExchange();
            _.bindAll(this, "notifyClients");
            this._registry = new PlatformAPIRegistry({
                clientNotifier: this.notifyClients
            });

            this._dispatcher = new MessageDispatcher({responder: this._registry});
            this._dispatcher.join(this._exchange.dispatchChannel());
            this._dispatcher.enable();

            transportStrategy.forward(this._exchange.egressChannel());
            this._exchange.listen(transportStrategy.dispatchChannel());
        },

        exchange: function() {
            return this._exchange;
        },

        notifyClients: function(eventName, args) {
            this._exchange.send({
                mType: "platformNotification",
                eventName: eventName,
                args: args
            });
        },

        registry: function() {
            return this._registry;
        }
    });

    return PlatformService;
};



if (typeof module !== "undefined" && module.exports) {
    module.exports = factory(
        require("underscore"),
        require("bit/commons/options"),
        require("bit/messaging/message-exchange"),
        require("bit/messaging/message-dispatcher"),
        require("bit/ext/core/platform/platform-api-registry")
    );
} else if (typeof define !== "undefined") {
    define([
        "underscore",
        "bit/commons/options",
        "bit/messaging/message-exchange",
        "bit/messaging/message-dispatcher",
        "bit/ext/core/platform/platform-api-registry"
    ], factory);
}
