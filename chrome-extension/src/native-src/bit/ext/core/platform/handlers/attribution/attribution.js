var factory = function(
    _,
    $options,
    $lang,
    FilteringDispatcher
) {

    var AttributionHandler = function() {
        this.initialize.apply(this, arguments);
    };

    _.extend(AttributionHandler.prototype, {
        initialize: function(opts) {
            opts = $options.fromObject(opts);
            // No peer controller!
            this._delegate = opts.getOrError("delegate");
            this._dispatcher = new FilteringDispatcher({
                namespace: "Attribution",
                delegate: this
            });
        },
        handle: function(ctx, api, argsObj, cb) {
            this._dispatcher.dispatch(ctx, api, argsObj, cb);
        },
        // Returns only information necessary for remote endpoint to
        // determine attribution data. Does not include ref or tag values
        onMsgGetRemoteAttributionParameters: function(ctx, args, cb) {
            this._delegate.getRemoteAttributionParameters(cb);
        }
    });

    return AttributionHandler;
};



if (typeof module !== "undefined" && module.exports) {
    module.exports = factory(
        require("underscore"),
        require("bit/commons/options"),
        require("bit/commons/lang"),
        require("bit/ext/core/platform/handlers/util/filtering-dispatcher")
    );
} else if (typeof define !== "undefined") {
    define([
        "underscore",
        "bit/commons/options",
        "bit/commons/lang",
        "bit/ext/core/platform/handlers/util/filtering-dispatcher"
    ], factory);
}


