/*
	SimpleStorage presents a consistent asynchronous proxy
	for each of our various native storage implementations, which
	are passed in as delegates.

	Each platform should implements its own NativeStorage conforming
	to the following interface:

	get(key, cb)
		cb has signature:
	  		cb(error, result)
	set(key, value, cb)
		cb has signature:
			cb(error)
	clear()

	@exports SimpleStorage class

*/

var factory = function() {

	var SimpleStorage = function() {
		this.initialize.apply(this, arguments);
	};

	SimpleStorage.prototype = {
		initialize: function(nativeStorage) {
			this._nativeStorage = nativeStorage;
		},
		get: function(k, cb) {
			this._nativeStorage.get(k, function(data) {
				cb(null, data[k]);
			});
		},

		set: function(k, v, cb) {
			var m = {};
			m[k] = v;
			this._nativeStorage.set(m, function() {
				cb();
			});
		},

		clear: function() {
			this._nativeStorage.clear();
		}
	};

	return SimpleStorage;

};

if (typeof module !== "undefined" && module.exports) {
    module.exports = factory();
} else if (typeof define !== "undefined") {
    define([], factory);
}


