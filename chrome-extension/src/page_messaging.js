(function() {
    "use strict";

    var port = chrome.extension.connect({name: "UBPAppsChromePage"});
    port.onMessage.addListener(function(response) {
        window.postMessage(response, '*');
    });
    
    var receiveMessage = function(message) {
        if (message.data && message.data.UBPMessageType === "UBPMessage") {
            delete message.error;
            delete message.success;
            port.postMessage(message.data);
        }
    };
    window.addEventListener('message', receiveMessage, false);
}());
